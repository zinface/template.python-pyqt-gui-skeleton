#!/usr/bin/env python3


import sys,os
sys.path.append(os.path.abspath(".")) 
sys.path.append(os.path.abspath("..")) 

from project.base import *
from project.ui_mainwindow import Ui_MainWindow


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        

if __name__ == '__main__':
    os.putenv(b"QT_QPA_PLATFORM", b"")
    
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()

    moveWidgetCenterInDesktopWidget(window)
    sys.exit(app.exec_())
