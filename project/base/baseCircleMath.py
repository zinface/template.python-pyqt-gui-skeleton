
from .basePointMath import *

class Circle:
    """
    ①圆周长: C=πd=2πr (d为直径,r为半径,π)
    ②三角形的周长C = a+b+c(abc为三角形的三条边)
    ③四边形:C=a+b+c+d(abcd为四边形的边长)
    ④特别的:长方形:C=2(a+b) (a为长,b为宽)
    ⑤正方形:C=4a(a为正方形的边长)
    ⑥多边形:C=所有边长之和。
    ⑦扇形的周长:C = 2R+nπR÷180˚ (n=圆心角角度) = 2R+kR (k=弧度)
    
    圆的周长: c=2πr=πd
    半圆的周长:c=πr+2r
    圆面积:S=πr²
    半圆的面积:S=(πr²)÷2
    圆环面积: S大圆 - S小圆=π(R²-r²)(R大圆半径)
    半圆周长: =πr+d
    """
    
    pi: int = math.pi
    origin: Point
    radius: float
    
    def __init__(self, origin: Point = Point(0,0), radius=1):
        """
        origin: 圆的坐标
        radius: 圆的半径
        """
        self.origin = origin
        self.radius = radius

    """
    圆的基本计算
    """
        
    # 来个圆直径计算
    def diameter(self,):
        """圆的直径计算
        - 2r:  2 * self.radius
        """
        return self.radius * 2
    
    # 来个圆周长计算
    def circumference(self):
        """圆的周长计算，默认使用 πd 计算
        - πd:  self.pi * self.diameter()
        - 2πr: 2 * self.pi * self.radius
        
        alias: perimeter()
        """
        return self.pi * self.diameter()
    
    def perimeter(self):
        """圆的周长计算，默认使用 2πr 计算
        - πd:  self.pi * self.diameter()
        - 2πr: 2 * self.pi * self.radius
        
        alias: circumference()
        """
        return mathPower(self.pi * self.radius, 2)
        
    # 来个圆的面积计算
    # I know how to calculate the area of a circle.
    def area(self):
        """圆的面积计算
        - πr²: (self.pi * self.radius) ** 2
        """
        return mathPower(math.pi * self.radius, 2)

    """
    圆弧、圆角度与弧计算
    """
    # 来个圆角弧线计算
    def arcForAngle(self, angle: float):
        """圆角弧线计算
        - arc=oπR/180
        """
        return angle * self.pi * self.radius / 180
        # return self.origin + self.radius * math.sin(angle / 180 * math.pi)
        # return math.pi * self.radius * math.cos(angle)

    # 来个圆角角度计算(?)
    def angleForArc(self, arc: float):
        """角度计算
        - arc=oπR/180:
        - angle(o)=πR/180/arc
        """
        return self.pi * self.radius / 180 / arc

    """
    同心圆之间的基本计算
    """
    def concentricCircles(self, radius):
        """构建一个不同半径的同心圆"""
        return Circle(self.origin, radius)

    # 同心圆的圆环面积计算
    def areaForConcentricCircle(self, target: ...):
        """同心圆的圆环面积计算
        - 此圆面积减去目标圆面积的绝对值
        
            abs(self.area() - target.area())
        """
        b: Circle = target
        return abs(self.area() - b.area())

    # 来个圆的一切计算，如果没有就写在这里
    
    
    
