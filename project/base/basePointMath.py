
from .baseMath import *

"""
基本的(坐标?)点计算
"""

class Point(object):
    x : int
    y : int
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    
    def distanceFor(self, target: ...):
        b: Point = target
        v1 = mathPower((self.x - b.x), 2)
        v2 = mathPower((self.y - b.y), 2)
        return abs(mathSqrt(v1 + v2))
    
    
    # def rotate(p1, p2, step):
    #     """计算围绕点2的步长值位置"""
    #     # x1, y1 = cw_rotate(p1.x, p1.y, step)
    #     # x2, y2 = cw_rotate(p2.x, p2.y, step)
    #     # return Point(x1, y1)
        

def math2PointDistance(a: Point, b: Point): 
    """计算两个点之间的距离

    Args:
        a (Point): 第一个点
        b (Point): 第二个点

    Returns:
        _type_: _description_
    """    
    v1 = mathPower((a.x - b.x), 2)
    v2 = mathPower((a.y - b.y), 2)

    return abs(mathSqrt(v1 + v2))

# 顺时针旋转
# def cw_rotate(x, y, ang):
#     ang = math.radians(ang)     # 将角度转换成弧度
#     # 用round()保留5位小数
#     new_x = round(x * math.cos(ang) + y * math.sin(ang), 5)
#     new_y = round(-x * math.sin(ang) + y * math.cos(ang), 5)
#     return new_x, new_y

