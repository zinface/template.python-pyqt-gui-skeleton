
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtNetwork import *


"""
基本的控件注册的 Helper 巨集

1. 基本控件注册
2. 基本控件事件注册
"""


class WidgetHelper():
    
    def registerWindowQuitButton(widget: QWidget, width: int = -1, height: int = -1):
        """registerFramelessWindowQuitButton
        
        为给定的对象进行注册无边框属性的退出按钮
        """
        widget._quitBtn = QPushButton("X", widget)
        
        if width == -1:
            widget._quitBtn.setFixedWidth(25)
            widget._quitBtn.setFixedHeight(25)
        elif height == -1:
            widget._quitBtn.setFixedWidth(width)
            widget._quitBtn.setFixedHeight(width)
        else:
            widget._quitBtn.setFixedWidth(width)
            widget._quitBtn.setFixedHeight(height)

        widget._quitBtn.clicked.connect(widget.close)
        widget._quitBtn.move(widget.width() - widget._quitBtn.width(), 0);

class WidgetEventHelper:
    
    def registerMoveEventNoBorder(widget: QWidget):
        return WidgetEventHelper.registerFramelessWindow(
            WidgetEventHelper.registerMoveEvent(widget)
        )
    
    def registerWA_TranslucentBackground(widget: QWidget):
        class _(widget):
            # def __init__(self, widget):
            #     super(widget, self).__init__(widget)
            #     self.setWindowFlag(Qt.FramelessWindowHint)
            
            def show(self):
                self.setAttribute(Qt.WidgetAttribute.WA_TranslucentBackground)
                super(widget, self).show()

        return _
    
    def registerFramelessWindow(widget: QWidget):
        """registerFramelessWindow
        
        为给定的对象进行注册无边框属性
        """
        
        class _(widget):
            # def __init__(self, widget):
            #     super(widget, self).__init__(widget)
            #     self.setWindowFlag(Qt.FramelessWindowHint)
            
            def show(self):
                self.setWindowFlag(Qt.FramelessWindowHint)
                super(widget, self).show()
                
        return _

    @staticmethod
    def registerMoveEvent(widget: QWidget):        
        """registerMoveEvent
        
        为给定的对象进行注册鼠标移动事件
        """
        class _(widget):
            
            m_drag: bool = False
            
            def mousePressEvent(self, e):
                if e.button() == Qt.LeftButton:
                    self.m_drag = True
                    self.m_DragPosition = e.globalPos() - self.pos()
                    e.accept()

            def mouseMoveEvent(self, e):
                if e.buttons() and Qt.LeftButton:
                    if self.m_drag:
                        self.move(e.globalPos() - self.m_DragPosition)
                        e.accept()
                        
        return _
        
        