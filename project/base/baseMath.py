# 基本的数学计算函数

import math

"""
基本的数学计算函数
"""
# 取和(加)
def mathAdd(a, b):
    return a + b

# 取差(减)
def mathSubtract(a, b):
    return a - b

# 取积(乘)
def mathMultiply(a, b):
    return a * b

# 取商(除)
def mathDivide(a, b):
    return a / b


"""
基本的数学计算函数
"""
# 取模
def mathMod(a, b):
    """
    与取余类似，但取整方式不同
        商 = 被除数 / 除数
        向下取整 = floor(商)
        模数 = 被除数 - 取整数 - (商品)除数
        
        例: mathMod(7,2)
    """
    return a % b

# 取余
def mathFloorDivide(a, b):
    """
    与取模类似，但取整方式不同
        商 = 被除数 / 除数
        向0取整 = fix(商)
        余数 = 被除数 - 取整数 - (商品)除数
        
        例: 
    """
    return a // b

# 取a的b次方
def mathPower(a, b):
    return pow(a, b)

# 取a的b次方的地板除
# def mathFloorPower(a, b):
#     return pow(a, b) // 2

# 取平方根
def mathSqrt(a):
    return math.sqrt(a)

# 取平方根的地板除
# def mathFloorSqrt(a):
#     return math.sqrt(a) // 2

