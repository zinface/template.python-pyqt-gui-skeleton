
import os
import sys
# print(sys.path)
# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 
# 对于 tests/subdir/ 中的内容，应该添加更深一层的父目录
sys.path.append(os.path.abspath("../..")) 
# print(sys.path)

from project.base import *

import project.resources

from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QMouseEvent

# 设计动态进度条?
from tests.widgets import ProgressMouseMoveBar

# MainWindow 一个来自 mainwindow.ui 的
from ui_mainwindow import Ui_MainWindow
class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.ui.label.setText("")
        # self.ui.label.setPixmap(QPixmap(":/assets/spark.png"))
        self.progress = ProgressMouseMoveBar(self)
        self.progress.setRange(0, 100)
        # self.progress.setValue(50)
        
        am = QPropertyAnimation(self.progress, b"value", self)
        am.setDuration(5000)
        am.setStartValue(0)
        am.setEndValue(100)
        am.start()
        
        self.progress.valueChanged.connect(lambda x: self.ui.label.setText("进度： %d %%" % (x)))
        
        self.setWindowTitle("动态进度条")

# 一个动画居中窗口
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    moveWidgetCenterInDesktopWidget(window)
    sys.exit(app.exec_())