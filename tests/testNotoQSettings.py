
import os
import sys

# print(sys.path)
# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 
# print(sys.path)

from project.base import *

# MainWindow 一个来自 mainwindow.ui 的
from ui_mainwindow import Ui_MainWindow

from PyQt5.QtCore import QSettings

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        # /home/zinface/.config/NotoQSettings/Core/Settings.conf
            # [Genera]
            # MainWindow=None
        settings = QSettings("NotoQSettings", "Core/Settings", self)
        settings.setValue("MainWindow","None")
        
        # /home/zinface/.config/NotoQSettings.conf
            # [Genera]
            # S=s
        QSettings("NotoQSettings").setValue("S","s")
        
        # /home/zinface/.config/NotoQSettings/Core/Settings.conf
            # [General]
            # MainWindow=None
        QSettings("NotoQSettings", "Core/Settings").value("MainWindow")
        # None
        
        # /home/zinface/.config/NotoQSettings/Core/Settings.conf
            # [General]
            # MainWindow=None
            #
            # [User]
            # zinface=password
        QSettings("NotoQSettings", "Core/Settings")\
            .setValue("User/zinface", "password")
        
        v = QSettings("NotoQSettings", "Core/Settings")\
            .value("User/zinface")
        # password
        
        # /home/zinface/.config/NotoQSettings/Core/Settings.conf
        # [General]
        # MainWindow=None
        #
        # [User]
        # zinface=password
        # zinface\age=18
        QSettings("NotoQSettings", "Core/Settings")\
            .setValue("User/zinface/age", "18")
        print(v)
        
# 一个动画居中窗口
if __name__ == '__main__':
    app = QApplication(sys.argv)
    #                  
    
    app.setOrganizationName("NotoQSettings")
    app.setApplicationName("Noto Application")
    window = MainWindow()
    window.show()
    animationMoveWidgetCenterInDesktopWidget(window)
    sys.exit(app.exec_())
