
import os
import sys

# print(sys.path)
# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 
# print(sys.path)

from PyQt5.QtGui import QPixmap

import project.resources
from project.base import *

# MainWindow 一个来自 mainwindow.ui 的
from ui_widgetshake import Ui_MainWindow

class Signaler(QObject):
    #无参数的信号
    signal1=pyqtSignal()
    #带一个参数（整数）的信号
    signal2=pyqtSignal(int)
    #带两个参数（整数，字符串）的信号
    signal3=pyqtSignal(int,str)
    #带一个参数（列表）的信号
    signal4=pyqtSignal(list)
    #带一个参数（字典）的信号
    signal5=pyqtSignal(dict)
    #带（整数 字符串）或者（字符串）的信号
    signal6=pyqtSignal([int,str],[str])
    
    def __init__(self, parent=None):
        super(Signaler, self).__init__(parent)

    def tigger(self):
        self.signal1.emit()
        self.signal2.emit(1)
        self.signal3.emit(1,"a")
        self.signal4.emit([1,2,3])
        self.signal5.emit({"name": "Signaler"})
        
        self.signal6[int,str].emit(1,"a")
        self.signal6[str].emit("b")
    
class MainWindow(QMainWindow):
    
    signer: Signaler

    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.signer = Signaler()
        self.signer.signal1.connect(lambda:              print('signer1 emit:'))
        self.signer.signal2.connect(lambda x:            print('signer2 emit:', x))
        self.signer.signal3.connect(lambda x,y:          print('signer3 emit:', x, y))
        self.signer.signal3.connect(lambda x,y:          print('signer3 emit:', x, y))
        self.signer.signal4.connect(lambda x:            print('signal4 emit:', x))
        self.signer.signal5.connect(lambda x:            print('signal5 emit:', x))
        self.signer.signal6[int,str].connect(lambda x,y: print('signal6 emit:', x,y))
        self.signer.signal6[str].connect(lambda x:       print('signal6 ovetload emit:', x))
        
        self.ui.shakeBtn.setText("Signaler")
        self.setWindowTitle("测试 Signaler 信号窗口")
        
    # 如果用 on_ 开头的写法，请不要 connect , 并且请修饰它
        # 或如果不使用 connect, 请使用 on_ 并修饰它
    # 如果不修饰它，请不要用 on_ 开头的写法，并且请使用 connect 连接
    @pyqtSlot()
    def on_shakeBtn_clicked(self):
        print("Press")
        self.signer.tigger()
        
        # Press
        # signer1 emit:
        # signer2 emit: 1
        # signer3 emit: 1 a
        # signer3 emit: 1 a
        # signal4 emit: [1, 2, 3]
        # signal5 emit: {'name': 'Signaler'}
        # signal6 emit: 1 a
        # signal6 ovetload emit: b

# 一个动画居中窗口
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    moveWidgetCenterInDesktopWidget(window)
    sys.exit(app.exec_())
