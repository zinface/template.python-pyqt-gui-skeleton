import sys,os

# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 

from project.base.baseCircleMath import *

p1 = Point(10,10)
p2 = Point(3,13)
distance = p1.distanceFor(p2)

print(distance)

c1= Circle(p1, 5)
c2= Circle(p1, 4)
c3= Circle(p1, 3)
c4= Circle(p1, 2)
c5= Circle(p1, 1)
c6= Circle(p1, 0)
print(c1.diameter())
print(c1.area())
print(c1.areaForConcentricCircle(c2))  # 28.27433388230814
print(c2.areaForConcentricCircle(c3))  # 21.991148575128552
print(c3.areaForConcentricCircle(c4))  # 15.707963267948966
print(c4.areaForConcentricCircle(c5))  # 9.42477796076938
print(c5.areaForConcentricCircle(c6))  # 3.141592653589793
# 1/2/3
# 
print(c1.angleForArc(5))
# print(c1.areaForCircle(c3))
# print(c2.areaForCircle(c3))