from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


class EllipseBackground(QWidget):
    
    centerPoint: QPoint
    
    def toggleNightMode(self):
        if self.timer.isActive():
            self.timer.stop()
            self.radius = 0
        else:
            self.timer.start()

    def updateRadius(self):
        self.radius += 10
        if self.radius >= 3000:
            self.timer.stop()
            self.radius = 3000
        self.update()
        
    def paintEvent(self, event):

        # 准备画板 - 抗锯齿 - 不透明
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setOpacity(1)
        
        # 新图层路径 - 椭圆
        path = QPainterPath()
        path.addEllipse(self.centerPoint, self.radius, self.radius)
        self.label_path = self.label_path.subtracted(path)

        # 画路径 - 0.8倍透明 - 画白色
        painter.fillPath(path, QColor(0, 0, 0))
        painter.setOpacity(0.8)
        # painter.fillPath(self.label_path, QColor(Qt.white))

        painter.end()
        
        super(QWidget, self).paintEvent(event)


def registerClickEllipseBackground(widget: QPushButton):
    class _(QPushButton):
        
        def __init__(self, parent):
            super(_, self).__init__(parent)
            self.radius = 0
            self.timer = QTimer()
            self.timer.setInterval(6)
            self.timer.timeout.connect(self.updateRadius)
            
            self.pressing = False
        
        def mousePressEvent(self, event: QMouseEvent) -> None:
            self.pressPoint = event.pos()
            self.pressing = True
            return super().mousePressEvent(event)
        
        def mouseReleaseEvent(self, event: QMouseEvent) -> None:
            self.pressing = False
            if self.pressPoint == event.pos():
                self.toggleNightMode()
            return super().mouseReleaseEvent(event)
        
        def toggleNightMode(self):
            if self.timer.isActive():
                self.timer.stop()
                self.radius = 0
            else:
                self.timer.start()

        def updateRadius(self):
            self.radius += 1
            if self.radius >= self.rect().width() * 2 and self.radius >= self.rect().height() * 2:
                self.timer.stop()
            self.update()
        
        def paintEvent(self, event):

            # 准备画板 - 抗锯齿 - 不透明
            painter = QPainter(self)
            painter.setRenderHint(QPainter.Antialiasing)
            painter.setOpacity(1)
            
            if self.pressing:
                # 新图层路径 - 椭圆
                path = QPainterPath()
                path.addEllipse(self.rect().center(), self.radius, self.radius)
                # self.label_path = self.label_path.subtracted(path)

                # 画路径 - 0.8倍透明 - 画白色
                painter.fillPath(path, QColor(0, 0, 0))
                painter.setOpacity(0.8)
                # painter.fillPath(self.label_path, QColor(Qt.white))
                
                path = QPainterPath()
                path.addEllipse(self.rect().center(), self.radius - 1, self.radius - 1)
                # self.label_path = self.label_path.subtracted(path)

                # 画路径 - 0.8倍透明 - 画白色
                painter.fillPath(path, QColor(Qt.white))
                painter.setOpacity(0.8)
                painter.end()
                
            print(self.geometry().center())
            
            super(_, self).paintEvent(event)
            
    return _
    

def registerPaintEvent(widget):
    
    class _(widget):
        def paintEvent(self, event):

            # 准备画板 - 抗锯齿 - 不透明
            painter = QPainter(self)
            painter.setRenderHint(QPainter.Antialiasing)
            painter.setOpacity(1)

            # 从按钮开始画 - 以本控件的中心为起点
            button_rect = self.button.geometry()
            # center_x = button_rect.x() + button_rect.width() / 2
            # center_y = button_rect.y() + button_rect.height() / 2
            center = button_rect.center()
            
            # 新图层路径 - 椭圆
            path = QPainterPath()
            # path.addEllipse(center_x - self.radius, center_y - self.radius, self.radius * 2, self.radius * 2)
            path.addEllipse(center, self.radius, self.radius)
            self.label_path = self.label_path.subtracted(path)

            # 画路径 - 0.8倍透明 - 画白色
            painter.fillPath(path, QColor(0, 0, 0))
            painter.setOpacity(0.8)
            # painter.fillPath(self.label_path, QColor(Qt.white))

            painter.end()
        
    return _