import sys
from PyQt5.QtCore import Qt, QTimer, QRectF
from PyQt5.QtGui import QColor, QPainter, QPainterPath
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QLabel


class Example(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setGeometry(300, 300, 250, 150)
        self.button = QPushButton('夜间测试', self)
        self.button.move(300, 800)
        self.button.clicked.connect(self.toggleNightMode)
        self.radius = 0
        self.timer = QTimer()
        self.timer.setInterval(6)
        self.timer.timeout.connect(self.updateRadius)
        self.label = QLabel("This is a label", self)
        self.label.setGeometry(78, 50, 1000, 300)
        self.label.setStyleSheet("font: 60px \"MiSans\"; color: white")
        self.label.setAlignment(Qt.AlignCenter)

        self.label_path = QPainterPath()
        self.label_path.addRect(QRectF(self.label.geometry()))

    def toggleNightMode(self):
        if self.timer.isActive():
            self.timer.stop()
            self.radius = 0
        else:
            self.timer.start()

    def updateRadius(self):
        self.radius += 10
        if self.radius >= 3000:
            self.timer.stop()
            self.radius = 3000
        self.update()

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setOpacity(1)

        button_rect = self.button.geometry()
        center_x = button_rect.x() + button_rect.width() / 2
        center_y = button_rect.y() + button_rect.height() / 2
        path = QPainterPath()
        path.addEllipse(center_x - self.radius, center_y - self.radius, self.radius * 2, self.radius * 2)
        self.label_path = self.label_path.subtracted(path)

        painter.fillPath(path, QColor(0, 0, 0))
        painter.setOpacity(0.8)
        painter.fillPath(self.label_path, QColor(Qt.white))

        painter.end()

    def drawLabel(self, painter):
        painter.save()
        painter.setClipPath(self.label_path)
        painter.drawText(self.label.geometry(), Qt.AlignCenter, self.label.text())
        painter.restore()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    ex.show()
    sys.exit(app.exec_())