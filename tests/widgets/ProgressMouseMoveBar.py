from PyQt5.QtWidgets import QProgressBar
from PyQt5.QtGui import QPixmap
from PyQt5.QtGui import QMouseEvent
from PyQt5.QtCore import pyqtSignal, pyqtSlot

# 设计动态进度条?
class ProgressMouseMoveBar(QProgressBar):
    """一个可由鼠标控件的动态的进度条
    1. 当鼠标点住并移动的时候改变进度条的值
    2. 通过内部的 isPress 来判断是否被按下
    """    
    
    mouseLeftEvent = pyqtSignal(bool)
    isPress: bool = False
    
    def __init__(self, parent=None):
        super(ProgressMouseMoveBar, self).__init__(parent)
        
        self.mouseLeftEvent[bool].connect(self.on_mouseLeftEvent)
        
    def mousePressEvent(self, event: QMouseEvent):
        self.mouseLeftEvent[bool].emit(True)
    
    def mouseReleaseEvent(self, event):
        self.mouseLeftEvent[bool].emit(False)
        
    def mouseMoveEvent(self, event: QMouseEvent) -> None:
        # print("Move")
        # 当
        if self.isPress == True:
            value = ((event.x() / self.width()) * 100)
            self.setValue(value)
            pass
        return super().mouseMoveEvent(event)

    @pyqtSlot()
    def on_mouseLeftEvent(self, pressing):
        print("pressing", pressing)
        self.isPress = pressing