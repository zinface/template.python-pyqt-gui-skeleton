from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSignal, pyqtSlot


class DropFile(QWidget):
    
    def __init__(self, parent: None):
        super(DropFile, self).__init__(parent)

        self.setAcceptDrops(True)
        
    def dropEvent(self, a0: QtGui.QDropEvent) -> None:
        return super().dropEvent(a0)
    
    def dragEnterEvent(self, a0: QtGui.QDragEnterEvent) -> None:
        return super().dragEnterEvent(a0)
    
    def dragLeaveEvent(self, a0: QtGui.QDragLeaveEvent) -> None:
        return super().dragLeaveEvent(a0)
    
    def dragMoveEvent(self, a0: QtGui.QDragMoveEvent) -> None:
        return super().dragMoveEvent(a0)
    
    
    
# TODO: Not Completed