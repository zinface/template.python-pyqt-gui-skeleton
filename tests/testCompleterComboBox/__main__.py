
"""testBaseManualWindow

本示例使用手动进行基本窗体的创建与初始化

1. 使用 QMainWindow 提供的 setCentralWidget 进行初始化
2. 对其 centralWidget 进行设置布局与其它布局控件

"""

import os
import sys

# print(sys.path)
# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 
# 对于 tests/subdir/ 中的内容，应该添加更深一层的父目录
sys.path.append(os.path.abspath("../..")) 
# print(sys.path)

from PyQt5.QtGui import QPixmap

# 引入由 pyrcc5 编译的 resources.qrc 生成的 resource.py 内容
# 虽然被 IDE 或 Python 语言服务器报告为未使用(unused)，但因为此模块内部资源是自加载
# 此模块在导入时会自动运行 qInitResources() 函数加载二进制资源(反正是自动的)
import project.resources
from project.base import *

# MainWindow 一个来自 mainwindow.ui 的
from ui_mainwindow import Ui_MainWindow


class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        
        self.setCentralWidget(QWidget())
        # self.label = QLabel("手动创建测试")
        
        self.lineEdit = QLineEdit(self)
        self.lineEdit.setPlaceholderText("Enter a value")
        
        self.completer = QCompleter(["apple", "banana", "cherry"])
        self.lineEdit.setCompleter(self.completer)
        
        self.comboBox = QComboBox(self)
        self.comboBox.addItems(["red", "green", "blue"])
        
        button = QPushButton("Submit", self)
        button.clicked.connect(self.submit)
        
        vbox = QVBoxLayout(self.centralWidget())
        # vbox.addWidget(self.label, 0, Qt.AlignmentFlag.AlignCenter)
        vbox.addWidget(self.lineEdit)
        vbox.addWidget(self.comboBox)
        vbox.addWidget(button)
        
        # self.label.setText("手动创建测试")
        # self.ui.label.setPixmap(QPixmap(":/assets/spark.png"))
        self.setWindowTitle("测试 手动创建 窗口")
        self.resize(440,312)

        self.setGeometry(300, 300, 250, 150)
        # self.setWindowTitle('QCompleter')
        
        # self.show()
        
    def submit(self):
        text = self.lineEdit.text()
        if text:
            print(f"You entered: {text}")
            print(f"Selected item: {self.comboBox.currentText()}")
            
        

# 一个动画居中窗口
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    moveWidgetCenterInDesktopWidget(window)
    sys.exit(app.exec_())
