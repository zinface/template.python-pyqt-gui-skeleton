
import os
import sys, requests, time

print(sys.path)
# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 
sys.path.append(os.path.abspath("../..")) 
# print(sys.path)

from PyQt5.QtGui import *

from project.base import *

from ui_mainwindow import *

scene = ""
from_w = ""

class MainWindow(QMainWindow, Ui_MainWindow):
    """
    {
        "id": 7241,
        "uuid": "92baae7f-4891-425e-81db-0163e91c2429",
        "hitokoto": "老夫年少时，也像他们一样，追随神的意志，征战四方。",
        "type": "c",
        "from": "王者荣耀",
        "from_who": "老夫子",
        "creator": "信仰需长存",
        "creator_uid": 9225,
        "reviewer": 4756,
        "commit_from": "web",
        "created_at": "1620515728",
        "length": 25
    }
    """
    
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)
        
        self.setAttribute(QtCore.Qt.WidgetAttribute.WA_TranslucentBackground, True)
        self.setWindowFlags(Qt.FramelessWindowHint)
        
        # self.frame_5.mousePressEvent = self.close_event
        # self.frame_6.mousePressEvent = self.min_event
        
        self.frame_5.mousePressEvent = lambda x: self.close()
        self.frame_6.mousePressEvent = lambda x: self.showMinimized()
        self.pushButton.clicked.connect(self.loader_hitokoto)
        
        self.loader_hitokoto(self)

    def loader_hitokoto(self, event):
        try:
            request = requests.get("http://v1.hitokoto.cn")
            result = request.text
            result = eval(result)
            global scene, from_w
            scene = result["hitokoto"]
            from_w = result["from"]
            self.label_5.setText(scene)
            self.label_6.setText(from_w)
            self.get_time(event)
            print("获取成功")
        except:
            self.loader_hitokoto(self)
            print("获取失败")

    def get_time(self, event):
        time1 = time.strftime("%m.%d  %I:%M", time.localtime())
        self.label_3.setText(time1)

    def mouseMoveEvent(self, e: QMouseEvent):
        try:
            self._endPos = e.pos() - self._startPos
            self.move(self.pos() + self._endPos)
        except:
            pass

    def mousePressEvent(self, e: QMouseEvent):
        if e.button() == Qt.LeftButton:
            self._isTracking = True
            self._startPos = QPoint(e.x(), e.y())

    def mouseReleaseEvent(self, e: QMouseEvent):
        if e.button() == Qt.LeftButton:
            self._isTracking = False
            self._startPos = None
            self._endPos = None


# 一个动画居中窗口
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    animationMoveWidgetCenterInDesktopWidget(window)
    sys.exit(app.exec_())
