import sys,os

# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 

from project.base.baseCircleMath import *


c = Circle(Point(0,0), 25)

print("c圆的半径:", c.radius)
print("c圆的直径:", c.diameter())
print("c圆的周长:", c.circumference())
print("c圆的面积:", c.area())
print("c圆的1角度弧线:", c.arcForAngle(1))
print("c圆的角度数:", c.angleForArc(c.arcForAngle(1)))

# c1 = Circle(Point(0,0), 50)
c1 = c.concentricCircles(50)

print("c1圆的半径:", c1.radius)
print("c1圆的直径:", c1.diameter())
print("c1圆的周长:", c1.circumference())
print("c1圆的面积:", c1.area())
print("c1圆的1角度弧线:", c1.arcForAngle(1))
print("c1圆的角度数:", c1.angleForArc(c1.arcForAngle(1)))

# print(isinstance(c, Circle))
# print(isinstance(c, Point))
# print(isinstance(c.origin, Point))
# print(c != None)