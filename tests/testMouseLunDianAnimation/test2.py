
"""testBaseManualWindow

本示例使用手动进行基本窗体的创建与初始化

1. 使用 QMainWindow 提供的 setCentralWidget 进行初始化
2. 对其 centralWidget 进行设置布局与其它布局控件

"""

import os
import sys

# print(sys.path)
# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 
# 对于 tests/subdir/ 中的内容，应该添加更深一层的父目录
sys.path.append(os.path.abspath("../..")) 
# print(sys.path)

from PyQt5.QtGui import QPixmap

# 引入由 pyrcc5 编译的 resources.qrc 生成的 resource.py 内容
# 虽然被 IDE 或 Python 语言服务器报告为未使用(unused)，但因为此模块内部资源是自加载
# 此模块在导入时会自动运行 qInitResources() 函数加载二进制资源(反正是自动的)
import project.resources
from project.base import *

# MainWindow 一个来自 mainwindow.ui 的
from ui_mainwindow import Ui_MainWindow

from PyQt5.QtGui import QMouseEvent, QPainter, QPaintEvent, QBrush, QPen, QCursor
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QBrush, QPen
from PyQt5.QtCore import Qt, QPoint, QTimer
import random
import sys


class CirclePoint:
    """圆的坐标
    Attributes:
        x (float):     圆心的x坐标
        y (float):     圆心的y坐标
        color (str):   圆的颜色，例如'red'、'blue'等
        radius (int):  圆的半径，单位为像素
        speed (float): 圆的运动速度，单位为秒/帧
        angle (float): 圆的角度，以弧度表示
    """
    def __init__(self, x, y, color):
        self.x = x
        self.y = y
        self.color = color
        self.radius = 5
        self.speed = 0.01
        self.angle = random.uniform(0, 2 * math.pi)

    def update(self, center_x, center_y):
        self.angle += self.speed                       # 根据速度更新角度
        self.x = center_x + math.cos(self.angle) * 50  # 根据当前角度计算新的x坐标
        self.y = center_y + math.sin(self.angle) * 50  # 根据当前角度计算新的y坐标

class MainWindow(QMainWindow):

    def __init__(self):
        super(MainWindow, self).__init__()
        
        self.setCentralWidget(QWidget())
        self.label = QLabel("手动创建测试")
        
        vbox = QVBoxLayout(self.centralWidget())
        vbox.addWidget(self.label, 0, Qt.AlignmentFlag.AlignCenter)
        
        # self.label.setText("手动创建测试")
        # self.ui.label.setPixmap(QPixmap(":/assets/spark.png"))
        self.setWindowTitle("测试 手动创建 窗口")
        self.resize(440,312)
        
        # self.mousePos = QPoint(0,0)
        self.initUI()
        
    def initUI(self):
        self.setGeometry(300, 300, 280, 170)
        self.setWindowTitle('Follow Mouse')
        self.circle_points = []
        self.color = Qt.red
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_points)
        self.timer.start()
        self.show()

    def paintEvent(self, e):
        painter = QPainter(self)
        painter.setPen(QPen(Qt.black, 1, Qt.SolidLine))
        for circle_point in self.circle_points:
            painter.setBrush(QBrush(circle_point.color, Qt.SolidPattern))
            painter.drawEllipse(QPointF(circle_point.x, circle_point.y), circle_point.radius, circle_point.radius)

    def update_points(self):
        mouse_pos = self.mapFromGlobal(QCursor.pos())
        if len(self.circle_points) == 0:
            for i in range(2):
                color = random.choice([Qt.red, Qt.green, Qt.blue, Qt.yellow, Qt.cyan, Qt.magenta, Qt.white])
                self.circle_points.append(CirclePoint(mouse_pos.x(), mouse_pos.y(), color))
        else:
            for i, circle_point in enumerate(self.circle_points):
                circle_point.update(mouse_pos.x(), mouse_pos.y())
        self.update()


# 一个动画居中窗口
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    moveWidgetCenterInDesktopWidget(window)
    sys.exit(app.exec_())





# class CirclePoint:
#     """圆的坐标
    
#     Attributes:
#         x (float): 圆心的x坐标
#         y (float): 圆心的y坐标
#         color (str): 圆的颜色，例如'red'、'blue'等
#         radius (int): 圆的半径，单位为像素
#         speed (float): 圆的运动速度，单位为秒/帧
#         angle (float): 圆的角度，以弧度表示
        
#     Methods:
#         update(center_x, center_y) -> None: 根据传入的圆心位置更新圆的位置和角度。
#     """
#     def __init__(self, x, y, color):
#         self.x = x  # 初始化圆心的x坐标
#         self.y = y  # 初始化圆心的y坐标
#         self.color = color  # 初始化圆的颜色
#         self.radius = 2  # 初始化圆的半径为2像素
#         self.speed = 0.01  # 初始化圆的运动速度为0.01秒/帧
#         self.angle = random.uniform(0, 2 * math.pi)  # 随机生成一个角度作为初始角度

#     def update(self, center_x, center_y):
#         """根据传入的圆心位置更新圆的位置和角度。"""
#         self.angle += self.speed  # 根据速度更新角度
#         self.x = center_x + math.cos(self.angle) * self.radius  # 根据当前角度计算新的x坐标
#         self.y = center_y + math.sin(self.angle) * self.radius  # 根据当前角度计算新的y坐标
