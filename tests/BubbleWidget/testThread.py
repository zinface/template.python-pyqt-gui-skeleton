
import os
import sys

# print(sys.path)
# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 
sys.path.append(os.path.abspath("../..")) 
# print(sys.path)

from PyQt5.QtGui import QPixmap

import project.resources
from project.base import *

# MainWindow 一个来自 mainwindow.ui 的
from ui_mainwindow import Ui_MainWindow


# Explicit and implicit alternation
class AnimationThread(QObject): 
    # 定义信号
    finished = pyqtSignal()
    changed = pyqtSignal(float)

    timeout: int = 1000
    timer: QTimer
    
    begin: float = 0
    end: float = 10
    step: float = 1
    tc: int = 0
    
    def __init__(self, parent=None):
        super(AnimationThread, self).__init__(parent)
        self.timer = QTimer(self)
        self.timer.timeout.connect(lambda : self.changed.emit(self.tc + self.step))
        self.changed.connect(self.clokcChanged)
        self.changed.connect(lambda x: print("changed:", x))
        

    def setRange(self, begin, end, step):
        self.begin = begin
        self.end = end
        self.step = step
        
    def setDuration(self, msecs):
        self.timeout = msecs

    def start(self):
        self.tc = self.begin
        self.timer.setInterval(self.timeout / abs((self.end - self.begin)/self.step))
        self.timer.start()

    def clokcChanged(self, x):
        self.tc = x
        if self.tc >= self.end:
            self.timer.stop()
            self.finished.emit()

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        # from widget import LmLmAnimation
        # LmLmAnimation()
        # a = Animation()
        # a.setTarget(self.ui.pushButton)
        # a.finished.connect(lambda: print("animation"))
        # a.start()
        
        self.ui.label.setText("测试 Animation 窗口")
        self.setWindowTitle(self.ui.label.text())
        
        # at = AnimationThread()
        # at.changed.connect(lambda x: print("changed:", x))
        # at.setRange(1,5,0.1)
        # at.start()
        
        

# 一个动画居中窗口
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    moveWidgetCenterInDesktopWidget(window)
    sys.exit(app.exec_())
