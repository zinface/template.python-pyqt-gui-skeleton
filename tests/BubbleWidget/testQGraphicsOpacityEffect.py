
import os
import sys

from widget import BubbleWidget

# print(sys.path)
# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 
sys.path.append(os.path.abspath("../..")) 
# print(sys.path)

from PyQt5.QtGui import QPixmap, QFontMetrics, QFont

import project.resources
from project.base import *

# MainWindow 一个来自 mainwindow.ui 的
from ui_mainwindow import Ui_MainWindow


class MainWindow(QMainWindow):
        
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # 图形不透明度效果，使用 ui 中的 pushButton 进行设置
        goe = QGraphicsOpacityEffect()
        goe.setOpacity(0.5)
        self.ui.pushButton.setGraphicsEffect(goe)
        
        
        self.ui.label.setText("测试 QGraphicsOpacityEffect 窗口")
        
        QTimer.singleShot(500, self.adjustLabelWidth)
        # self.ui.label.setMinimumSize(0,0)
        self.setWindowTitle(self.ui.label.text()) 
        
        
    @pyqtSlot()
    def on_pushButton_clicked(self):
        pass
        
        # rect = self.ui.label.rect()
        # rect.setY(self.ui.pushButton.y() + self.ui.pushButton.height() + 10)
        # self.ui.label.setGeometry(rect)
        
        # self.ui.label.geometry().setY(self.ui.label.y() + 10)

    @pyqtSlot()
    def adjustLabelWidth(self):
        font = self.ui.label.font()
        fm = QFontMetrics(font)
        textWidth = fm.width(self.ui.label.text())
        self.ui.label.setMinimumSize(QSize(textWidth, 0))
        animationMoveWidgetCenterInWidget(self.ui.label, self)

# 一个动画居中窗口
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    moveWidgetCenterInDesktopWidget(window)
    sys.exit(app.exec_())
