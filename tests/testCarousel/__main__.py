
import os
import sys

# print(sys.path)
# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 
# 对于 tests/subdir/ 中的内容，应该添加更深一层的父目录
sys.path.append(os.path.abspath("../..")) 
# print(sys.path)

from PyQt5.QtGui import QPixmap

import project.resources
from project.base import *

# MainWindow 一个来自 mainwindow.ui 的
from ui_widgetcarousel import Ui_Carousel


class MainWindow(QMainWindow):
    """目的是在这个窗口中来实现旋转木马的效果，但目前还无法正常实现相关的操作
    """
    
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_Carousel()
        self.ui.setupUi(self)
        
        pixmap = QPixmap(":/assets/spark.png")
        
        self.ui.label.setText("")
        self.ui.label.setPixmap(QPixmap(":/assets/spark.png"))
        self.ui.label_2.setPixmap(QPixmap(":/assets/spark.png"))
        self.ui.label_3.setPixmap(QPixmap(":/assets/spark.png"))
        
        self.resetLabelSize(self.ui.label, pixmap)
        self.resetLabelSize(self.ui.label_2, pixmap)
        self.resetLabelSize(self.ui.label_3, pixmap)
        
        QTimer.singleShot(1000, self.singleShotSlot)

        self.setWindowTitle("测试 Carousel 窗口")
        
    def resetLabelSize(self, widget: QWidget, pixmap: QPixmap): 
        widget.setGeometry(widget.x(), widget.y(), pixmap.width(), pixmap.height())
        self.widgetBaseLine(widget)
        
    def widgetBaseLine(self, widget):
        y = (self.height() - widget.height()) / 2
        widget.setGeometry(widget.x(), y, widget.width(), widget.height())
        
    def AnimationLeft(self, widget: QLabel): 
        animation = QPropertyAnimation(widget, b'rect', widget)
        # pos = widget.pos()
        # pos.setX(pos.x() - widget.width() * 0.8)
        # pos.setY(pos.y() + widget.height() * 0.2)
        
        # rect = widget.rect()
        # rect.setX(rect.x() - widget.width() * 0.8)
        # rect.setY(rect.y() + widget.height() * 0.2)
        # rect.setWidth(widget.width() * 0.8)
        # rect.setHeight(widget.height() * 0.2)
        
        
        animation.setDuration(1000)
        animation.setStartValue(widget.rect())
        # animation.setEndValue(widget.)
        
        size = widget.size()
        width =size.width() * 0.8
        height = size.height() * 0.8
        pixmap = widget.pixmap().scaled(width, height, aspectRatioMode=Qt.AspectRatioMode.KeepAspectRatio, transformMode=Qt.TransformationMode.SmoothTransformation)
        widget.setGeometry(QRect(widget.pos(),pixmap.size()))
        widget.setPixmap(pixmap)
        self.widgetBaseLine(widget)
        
    def singleShotSlot(self):
        self.AnimationLeft(self.ui.label_2)

# 一个动画居中窗口
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    moveWidgetCenterInDesktopWidget(window)
    sys.exit(app.exec_())
