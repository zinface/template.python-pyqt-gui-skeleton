
import os
import sys

# print(sys.path)
# 由于本脚本在 tests 目录，任何情况下将只以此脚本所在目录为根目录
# 如果是使用 python3 运行此脚本，确保在编译器的时候加入当前目录(确保在项目目录下)
sys.path.append(os.path.abspath(".")) 
# 如果是使用 Code Runner 插件运行此脚本，确保在 cd <paths>/test 的时候加入父级目录
sys.path.append(os.path.abspath("..")) 
# print(sys.path)

from PyQt5.QtGui import QPixmap

import project.resources
from project.base import *

# MainWindow 一个来自 mainwindow.ui 的
from ui_widgetshake import Ui_MainWindow

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.setWindowTitle("测试控件抖动窗口")
        
    # 如果用 on_ 开头的写法，请不要 connect , 并且请修饰它
        # 或如果不使用 connect, 请使用 on_ 并修饰它
    # 如果不修饰它，请不要用 on_ 开头的写法，并且请使用 connect 连接
    @pyqtSlot()
    def on_shakeBtn_clicked(self):
        print("Press")
        shakeBtn = self.ui.shakeBtn
        
        x = shakeBtn.x()
        y = shakeBtn.y()
        shakeBtnPoint = QPoint(x,y)
        shakeBtnSize = shakeBtn.size()
        shakeBtnRect = QRect(shakeBtnPoint, shakeBtnSize)
        
        # 使用动画来操作目标的属性
        animation = QPropertyAnimation(shakeBtn, b"geometry", self)
        animation.setEasingCurve(QEasingCurve.Type.InOutSine)
        animation.setDirection(400)
        animation.setStartValue(shakeBtnRect)
        animation.setEndValue(shakeBtnRect)
        
        # 定义抖动幅度、以及次数和单步数值比例计算
        shakeRange = 10     # 我们将其固定(也许可以使用传参形式)
        shakeCount = 8      # 我们将其固定抖动次数为8次
        shakeStep = 1.0/shakeCount  # 我们将计算步值，用于 setKeyValueAt 使用的 0-1 之间的步值
        for i in range(1, shakeCount): 
            shakeRange = -shakeRange
            animation.setKeyValueAt(shakeStep * i, QRect(QPoint(x + shakeRange, y), shakeBtnSize))
        animation.start(QAbstractAnimation.DeletionPolicy.DeleteWhenStopped)

# 一个动画居中窗口
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    moveWidgetCenterInDesktopWidget(window)
    sys.exit(app.exec_())
