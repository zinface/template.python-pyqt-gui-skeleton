from setuptools import setup, find_packages

# 版本号，来自 VERSION 文件
with open("VERSION") as f:
    version = f.read()

# 说明，来自 README.md 文件, 不知道内容是否符合规范
with open('README.md') as f:
    readme = f.read()

# 许可证，来自 LICENSE.txt 文件
with open('LICENSE.txt') as f:
    license = f.read()

setup(
    name='project',                 # 应用名
    version=version,                # 版本号
    packages=find_packages(exclude=('tests', 'docs')),
                    # find_packages
                    #   查找当前目录下存在 py 文件的目录，并以包目录而名
                    #   例如： 此项目中 project 目录、tests 目录中都有 py 文件，而 project 目录才是主要程序所在目录(有 __main__.py 文件)
                    #         主要程序入口虽然以 __main__.py 命名:
                    #           但我们在使用的时候不会经常以 python3 -m project 方式运行，而是需要像执行命令一样去执行: 
                    #              例如 project 命令
                    #              所以可以使用 scripts=["bin/project"] 配置
                    # 包括在安装包内的包 (例如在'项目文件夹'下的'以项目名称命名的目录')
                    # 如了解 '项目目录'与'项目名称命名的目录':
                    #   在 docs/project-skeleton.md 中 '来自 project 的说明' 小结有明确说明
    
    scripts=["bin/project"],

    description='python-pyqt-gui-skeleton 骨架模板',
    long_description=readme,
    
    # 作者，邮箱，开源项目地址
    author='zinface',
    author_email='zinface@163.com',
    url='https://gitee.com/zinface/python.python-pyqt-gui-skeleton',
    
    # 许可证？文件内容还是直接 = "GPLv3"?
    license=license,
    
    # install_requires=["PyQt5==5.15.7"] # 注释掉，省事
)

######################### 关于 find_packages 的简单作用 #########################
# $ python3
# >>> from setuptools import find_packages
#

# >>> find_packages()
# ['tests', 'project', 'project.widgets', 'project.base']
#  ^不需要的内容

# 所以可以使用 exclude 去掉不需要的包(目录)
# >>> find_packages(exclude=('tests', 'docs'))
# ['project', 'project.widgets', 'project.base']
# 

######################### 关于 可执行文件(bin/project) 的简单说明 #########################
# 如果要使用 bin/project(一个软链接到 project/__main__.py 的文件) 作为一个实际使用的命令行程序
# project/__main__.py 文件内所有的 from/import 引入的库都将需要明确指定来源于 '以 project 命名的项目'
# 例如:
#   我们在使用/测试/编写时并未安装到系统的项目:
#       from base.baseQt import *   # project 项目的子模块
#       from widgets import *       # project 项目的子模块
#       import resource             # qrc 资源文件编译出的 resource.py

#   以上内容如果在 __main__.py 文件中，并且被外部当作主程序入口使用，那么都需要改为:
#       from project.base.baseQt import *   # project 项目的子模块
#       from project.widgets import *       # project 项目的子模块
#       import project.resource             # qrc 资源文件编译出的 resource.py

#   就像是在外部程序中使用这个项目的库一样编写.


######################### 关于 setup.py 的 requires 的简单说明(当然貌似没什么用，我还是不参考以下内容了) #########################

# 一些 requires 可参考: 
#   https://note.qidong.name/2018/01/python-setup-requires/


# NOTE:
#   一般，平时使用 pip 安装包时使用的 pipy 源是
#       global.index-url='https://mirrors.bfsu.edu.cn/pypi/web/simple'
# [global]
# index-url='https://mirrors.bfsu.edu.cn/pypi/web/simple'
# 
# pip3 config set global.index-url 'https://mirrors.bfsu.edu.cn/pypi/web/simple'
# sudo pip3 config set global.index-url 'https://mirrors.bfsu.edu.cn/pypi/web/simple'

# NOTE:
# 而 setup() 函数的这些 require，尤其是 tests_require 和 setup_requires，是通过 easy_install 来安装的
# 
# [easy_install]
# index-url=http://mirrors.aliyun.com/pypi/simple/
# 
# pip3 config set easy_install.index-url https://mirrors.bfsu.edu.cn/pypi/web/simple
# sudo pip3 config set easy_install.index-url https://mirrors.bfsu.edu.cn/pypi/web/simple
