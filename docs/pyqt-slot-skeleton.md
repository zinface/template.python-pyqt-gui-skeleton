# Pyhont Qt 项目槽函数的定义与使用预览

- 一个 Python Qt 项目中的槽函数定义结构

    > 在 ui 文件中包含一个名称为 shakeBtn 的 QPushButton，下面是为此按钮设计的槽函数内容

    ```python
    class MainWindow(QMainWindow):
        def __init__(self):
            super(MainWindow, self).__init__()
            self.ui = Ui_MainWindow()
            self.ui.setupUi(self)
            
            # self.ui.shakeBtn.clicked.connect(self.on_shakeBtn_clicked)
            self.ui.shakeBtn.clicked.connect(self.shakeBtn_clicked)
            self.setWindowTitle("测试控件抖动窗口")
            
        # 如果用 on_ 开头的写法，请不要 connect , 并且请修饰它
            # 或如果不使用 connect, 请使用 on_ 并修饰它
        # 如果不修饰它，请不要用 on_ 开头的写法，并且请使用 connect 连接
        # def on_shakeBtn_clicked(self):
        @pyqtSlot()
        def shakeBtn_clicked(self):
            print("Press")


    # 关于 on_ 开头的函数名称
        # 是我们在 C++ Qt 中惯用的槽函数命名(意味着按照 Qt 原生命名规范进行命名)
        # 但是在 PyQt 中，需要注意这种写法需要使用 @pyqtSlot() 进行修饰

    # 关于 connect 连接信号与槽函数
        # 这是 Qt 中的核心机制，在 Qt 中用 connect 将互不相关的对象绑定在一起，实现对象之间的通信
        # 在 PyQt 中，connect 规则为:
            # (控件名称).(信号名称).connect(槽函数名称)
            # 有参数时，槽函数名称部分写成：lambda 参数名：函数名(参数名)
                self.ui.shakeBtn.clicked.connect (lambda x: self.shakeBtn_clicked(x)) 
                # ^ 仅参考示例(默认按钮点击不存在参数)

    # 1. 第一种情况，仅定义 on_shakeBtn_clicked 槽函数(不修饰，不连接)
    
        # Press
        # Press

    # 2. 第二种情况, 定义 on_shakeBtn_clicked 槽函数(仅连接)
        # 并使用 connect 连接到 on_shakeBtn_clicked 槽函数

        # Press
        # Press
        # Press

    # 3. 第三种情况，定义 on_shakeBtn_clicked 槽函数(仅修饰)
        # 并使用 @pyqtSlot() 修饰
        
        # Press

    # 4. 第四种情况，定义 shakeBtn_clicked 槽函数(仅连接)
        # 使用 connect 连接到 shakeBtn_clicked 槽函数
   
        # Press
    
    # 5. 第五种情况，定义 shakeBtn_clicked 槽函数(修饰，连接)
        # 使用 @pyqtSlot() 修饰
        # 使用 connect 连接到 shakeBtn_clicked 槽函数

        # Press

    # ...

    # 以上不分先后(正确或错误写法)
    # 最后:
        #    on_ 开头函数的写法参看 '来源于 @pyqtSlot()' 节说明
        # 非 on_ 开头的函数与 connect 参看 '来源于 connect()' 节说明
    ```


- 来源于 `@pyqtSlot()` 修饰符定义的槽函数说明

    > @pyqtSlot()有两种使用情况，如下两个例子

    ```python
    # 例子1：
    # 一个信号时，如 pushButton 的 clicked，即按钮的点击信号
    
    # 需要引入 pyqtSlot 库函数
    from PyQt5.QtCore import pyqtSlot

    # 信号与槽函数
    @pyqtSlot()
    def on_shakeBtn_clicked(self):
        print("Press")
        
    # @pyqtSlot() 是这种方式修饰关键词，他表明下面函数是信号槽函数
    # 在 @pyqtSlot() 方式里，函数名称有特殊要求:
    #   on_(控件对象名)_信号名(self,内置参数)
    ```

    ```python
    # 例子2：
    # 一个控件多个信号时：
    # 如名为 self.lineEdit_2 的 QlineEdit 控件的 returnPressed 和 textChanged 信号

    @pyqtSlot()
    def on_lineEdit_2_returnPressed(self):
        print('触发了信号 returnPressed')

    def on_lineEdit_2_textChanged(self):
        print('触发了信号 textChanged')
    
    # @pyqtSlot() 在一个控件上不管有多少信号与槽函数，它都只写一次(如上)
    #    但在有多个控件的情况下， 需要为每一个控件都最少编写一次 @pyqtSlot()
    #
    # 注：完整的例子代码是在一个类中，所以有关键字 self
    ```

- 来源于 connect() 的槽函数定义说明

    ```python
    # 在初始化函数中信号连接
    self.pushButton.clicked.connect(self.A)

    # 槽函数
    def A(self):
        print('点击了按钮')

    # 由于信号的连接和槽函数是分开的，所以槽函数名称没有特殊要求。
    # 
    # 而信号连接语句有特殊要求
    #   (控件名称).(信号名称).connect(槽函数名称)
    #   有参数时，槽函数名称部分写成lambda 参数名：函数名(参数名)
    #
    # 注意，没有参数时槽函数不用写双括号()
    # 一个控件多个信号时写法也一样。
    ```


- 来源于 C++ Qt 中的槽函数写法

    > 随着 Qt 核心版本发展，在程序中对于信号与槽函数的写法也进行了多次修正

    ```c++
    QPushButton *btn = new QPushButton;

    // 方式一：老式写法
    connect(btn, SIGNAL(clicked()), this, SLOT(close()));
        // 在编译的时候即使信号或槽不存在也不会报错，但是在执行的时候无效
        // 对于C++这种静态语言来说，这是不友好、且不利于调试

    // 方式二：Qt5后推荐的写法
    connect(btn, &QPushButton::clicked, this, &MainWindow::close);
        // 如果编译的时候信号或槽不存在是无法编译通过的，相当于编译时检查，不容易出错
        // 其次是槽的写法可以直接写在 public 控制域下，不一定非要写在 public slots: 控制域下

    // 方式三：采用了 lambda 表达式的写法
    connect(btn, &QPushButton::clicked, this, [&]() {
        this->close();
    });
        // 更加方便快捷。

        // 关于lambda需要注意一点：
        QTimer::singleShot(3000, /*this,*/ [&]{
            this->close();      // ^ 这是可省略的
        });

        connect(btn, &QPushButton::clicked, /*this,*/ [&]() {
            this->close();                 // ^ 这是可省略的
        });
        // 在使用 lambda 表达式的时候
        // 槽的接收者QObject是可以省略不写的，这时候 Qt 会默认发射者与接收者属于同一个QObject；


        // 这是连接信号与槽函数的模板函数
        template <typename Func1, typename Func2>
        static inline typename std::enable_if<QtPrivate::FunctionPointer<Func2>::ArgumentCount == -1, QMetaObject::Connection>::type
            connect(const typename QtPrivate::FunctionPointer<Func1>::Object *sender, Func1 signal, Func2 slot)
        {
            return connect(sender, signal, sender, slot, Qt::DirectConnection);
        }


        // 当我们省略槽函数接收者QObject时，那我们就必须要注意lambda内成员的生命周期。
        // 例如示例的 singleShot
            // 若在槽函数响应前 this 已经销毁变为无效联依然存在，指针，后果就会很严重!!!

        // Why?
        // 我们知道，connect 的发射者与接收者任意一个销毁，那么这个 connect 就已经断开了。
        // 而当我们省略接收者QObject的时候，发射者与接收者属于同一个QObject。
        // 在示例中，信号槽 connect 关联依然存在，信号槽依然会触发，但此时 this 已经被销毁就不好玩了。

    ```