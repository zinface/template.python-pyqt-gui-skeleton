# tests 测试示例骨架预览

- [一个 python 项目基本主体结构](project-skeleton.md)

- 补充，来源于 tests 的基本主体结构

    ```
    .
    ├── BubbleWidget
    │   ├── mainwindow.ui
    │   ├── README.md
    │   ├── testAnimation.py
    │   ├── testIcon.py
    │   ├── testQGraphicsOpacityEffect.py
    │   ├── testThread.py
    │   ├── testWidget.py
    │   ├── ui_mainwindow.py
    │   └── widget.py
    ├── Hitokoto
    │   ├── 20221229155847.png
    │   ├── main.py
    │   ├── mainwindow.ui
    │   ├── __pycache__
    │   │   ├── ui_mainwindow.cpython-37.pyc
    │   │   └── ui_untitled.cpython-37.pyc
    │   ├── README.md
    │   └── ui_mainwindow.py
    ├── __init__.py
    ├── mainwindow.ui
    ├── progressbar.ui
    ├── ProgressMouseMoveBar.py
    ├── __pycache__
    │   ├── ui_mainwindow.cpython-37.pyc
    │   ├── ui_testMultiComboBox.cpython-37.pyc
    │   ├── ui_widgetcarousel.cpython-37.pyc
    │   └── ui_widgetshake.cpython-37.pyc
    ├── tempCodeRunnerFile.py
    ├── testBaseQtAnimationMoveCenter.py
    ├── testBaseQtMoveCenterInDesktopWidget.py
    ├── testCarousel.py
    ├── testCircleMath.py
    ├── testCustomeProgressBarWidget.py
    ├── testIcon
    │   └── __main__.py
    ├── testMath.py
    ├── testMultiComboBox
    │   ├── __main__.py
    │   ├── ui_testMultiComboBox.py
    │   └── ui_testMultiComboBox.ui
    ├── testProgress.py
    ├── testSignals.py
    ├── testWidgetShake.py
    ├── ui_mainwindow.py
    ├── ui_progressbar.py
    ├── ui_widgetcarousel.py
    ├── ui_widgetshake.py
    ├── widgetcarousel.ui
    └── widgetshake.ui

    6 directories, 44 files
    ```

- 一些说明

    - 来自 __pycache__ 的说明

        ```python
        # __pycache__ 是 Python3 解释器提供的快速使用预编译的源代码文件加速启动程序的特性
        # 如果你不希望解释器生成此文件夹，可以为解释器添加 -B 参数抑制此特性。

        # 如果希望永久关闭此特性可以使用：PYTHONDONTWRITEBYTECODE= 环境变量 
        # 这个设置可能会使程序运行速度变慢。
        ```

    - 来自 testIcon/__main__.py 的说明

        ```python
        # 对于 tests/subdir/ 中的内容，应该添加更深一层的父目录
        sys.path.append(os.path.abspath("../.."))

        # 并无其它内容
        ```