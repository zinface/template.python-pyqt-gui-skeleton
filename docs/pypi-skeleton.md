# PyPi 或称 pip 源配置预览

1. 官方源配置(下载较慢)

    ```shell
    # 使用命令配置
    pip config set global.index-url http://pypi.org/simple

    # 在执行 pip 安装时指定
    pip install pip -i http://pypi.org/simple
    ```

2. 国内镜像站配置(快，但也得看实际镜像站速度)

    ```shell
    # 清华源
    pip config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple

    # 北京外国语大学开源软件镜像站
    pip config set global.index-url https://mirrors.bfsu.edu.cn/pypi/web/simple

    # 阿里源
    pip config set global.index-url https://mirrors.aliyun.com/pypi/simple/

    # 腾讯源
    pip config set global.index-url http://mirrors.cloud.tencent.com/pypi/simple

    # 豆瓣源
    pip config set global.index-url http://pypi.douban.com/simple/
    ```