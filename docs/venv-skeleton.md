# python 虚拟环境管理工具 venv 预览

- 来源于 python3 创建的 venv 环境预览

    ```shell
    # 创建 env 环境
    $ python3 -m venv pyenv
    $ tree pyenv/ -L 2

    pyenv/
    ├── bin
    │   ├── activate                # Linux Bash 激活脚本，source 运行
    │   ├── activate.csh
    │   ├── activate.fish
    │   ├── easy_install
    │   ├── easy_install-3.7
    │   ├── pip
    │   ├── pip3
    │   ├── pip3.7
    │   ├── python -> python3
    │   └── python3 -> /usr/bin/python3
    ├── include
    ├── lib
    │   └── python3.7
    ├── lib64 -> lib
    ├── pyvenv.cfg
    └── share
        └── python-wheels

    7 directories, 11 files
    ```

- 来源于 venv 的基本预览

    ```python
    # 在 Python 中有各种各样的系统包和第三方开发的包，这使我开发变得异常容易。
    # 但此时也引入了一个问题，在不同代码，或不同项目，以及在 requirements.txt 中，
    # 声明的依赖、或需要的包版本可能是不一样的。

    # 在这种情况下，为了当前的项目的正确开发与运行调整了依赖库的版本，有可能会导致曾经
    # 创建过的项目或代码无法正常工作。

    # 因此经常需要对不同的代码来设置不同的 Python 运行环境。
    # 而 venv 是 Python 自带的虚拟环境管理工具，使用非常方便。

    # 但需要注意的是，venv 工具无法创建不同版本的 Python 环境，也就是你在 Python3.5 中
    # 使用的 venv 将无法创建 Python3.6 或其它版本的的虚拟环境。
    
    # 如果你想要使用不同 Python 版本的虚拟环境，可以使用真正的虚拟运行环境，并单独创建例如
    # py35env、py36env、py37env 等独立的开发环境，可以了解 virtualenv，它是 venv 的更高级工具。
    
    # 其它: 基于 VSCode 插件实现的 PlatformIO 嵌入式开发平台所使用的 Python 独立环境就是 virtualenv。
    ```

- 来源于 venv 的安装与激活和便用的预览

    1. 安装

        ```shell
        # python3.6 及以上已经默认安装，python3.5 需要通过系统的包管理工具安装：
        sudo apt install python3-venv
        ```

    2. 创建虚拟环境

        ```shell
        # 在 ~/test_env 目录下创建虚拟环境
        python3 -m venv py3env
        ```

    3. 激活虚拟环境
        
        ```shell
        # 在Linux 和 Mac 环境下，执行下面的命令：
        source ~/py3env/bin/activate
        
        # 在Windows环境下，执行下面的命令:
        .\py3env\bin\activate.bat

        # 可以看到，命令行的提示符前面会出现括号，里面是虚拟环境名称。
        $ source pyenv/bin/activate
        (pyenv) $ 
        ```

        ```shell
        # 使用pip安装需要的包：
        pip install tensorflow

        # 注意这里不需要 root 权限，因此无需添加 sudo。
        # 安装的包会放在 ~/pyenv/lib/pythonx.x/site-packages 目录下。
        ```

    4. 退出虚拟环境

        ```shell
        # 退出虚拟的python环境，在命令行执行下面的命令即可：
        deactivate

        # 可以看到，在命令行的提示符前面出现的虚拟环境标志在执行命令后将被还原。
        (pyenv) $ deactivate
        $
        ```
