CPUS=$(shell nproc)

PROJECT_NAME=project
PROJECT_VERSION=$(shell cat VERSION)

all:
	@echo 无事可做

run: pyuic
	@echo "是否存在 resource.qrc? 是否已经使用 make pyrcc 生成?" 
	@echo "正在运行."
	@/usr/bin/python3 $(PROJECT_NAME)

run-tests:
	/usr/bin/python3 tests/testBaseQtAnimationMoveCenter.py
	/usr/bin/python3 tests/testBaseQtMoveCenterInDesktopWidget.py
	/usr/bin/python3 tests/testCircleMath.py
	/usr/bin/python3 tests/testCustomeProgressBarWidget.py
	/usr/bin/python3 tests/testIcon.py
	/usr/bin/python3 tests/testMath.py
	/usr/bin/python3 tests/testProgress.py
	/usr/bin/python3 tests/testWidgetShake.py

clean:
	/usr/bin/py3clean .

# 判断 pipreqs 是否存在，存在即可使用 pipreqs 进行导出 requirements.txt
PIPREQS="$(shell which pipreqs)"
ifneq ($(PIPREQS), "")
requirements-export:
	@echo "正在导出 requirements.txt"
	@pipreqs

requirements-export--force:
	@echo "正在导出 requirements.txt(使用 --force 参数覆盖)"
	@pipreqs --force

else
requirements:
	@echo "您是否未安装 pipreqs? 请使用以下命令来安装(否则请查看 Makefile):"
	@echo "    python3 -m pip install pipreqs"

endif

################### 添加的其它操作(用于处理 pyuic 内容) ###################
# 是否应该将例如 mainwindow.ui > ui_mainwindow.py? 也许比较符合 Qt 规范。
# 是否应该定义例如 uic-target 宏，用于批量处理 pyuic 操作
define uic-target
UI_TARGET=$(1)
UI_NAME=$$(shell basename -s .ui $(1))
UIC_TARGET=$$(shell echo $(1) | sed 's/.ui$/.py/')
endef

UI_RESOURCES := $(PROJECT_NAME)/ui_mainwindow.ui			\
				tests/testBaseWindow/ui_mainwindow.ui 		\
				tests/testIcon/ui_mainwindow.ui


UIC_RESOURCES := $(UI_RESOURCES:.ui=.py)
# $(foreach n,$(UI_RESOURCES),$(eval $(call uic-target,$(n))))
uics:
	@for i in $(UI_RESOURCES); do\
		test -f $$i || continue;\
		echo "$$i ----> $$(echo $$i | sed 's/.ui$$/.py/')";\
		# pyuic5 $$i > $$(echo $$i | sed 's/.ui$$/.py/');\
	done

show-uics:
	@for i in $(UI_RESOURCES); do\
		test -f $$i || continue;\
		echo $$i;\
	done

# test find/run __main__.py
# MAIN_SRCS := $(wildcard tests/*/__main__.py)
MAIN_SRCS := tests/testBaseWindow	\
			 tests/testIcon

show-mainsrcs:
	@# echo $(MAIN_SRCS)
	@for i in $(MAIN_SRCS); do echo $$i; done

run-mainsrcs:
	@for i in $(MAIN_SRCS); do python3 $$i; done

# test ui->uic
UI_SRCS:=$(wildcard tests/*.ui)
UIC_SRCS:=$(UI_SRCS:.ui=.py)

show-uicsrcs:
	# echo $(UIC_SRCS)
	@for i in $(UIC_SRCS); do echo $$i; done

mainwindow:
	test $(PROJECT_NAME)/mainwindow.ui && pyuic5 $(PROJECT_NAME)/mainwindow.ui > $(PROJECT_NAME)/ui_mainwindow.py

test-mainwindow:
	test tests/mainwindow.ui && pyuic5 tests/mainwindow.ui > tests/ui_mainwindow.py

test-customeProgressbar:
	pyuic5 tests/progressbar.ui > tests/ui_progressbar.py

test-widgetshake:
	pyuic5 tests/widgetshake.ui > tests/ui_widgetshake.py

test-bubblewidget-mainwindow:
	pyuic5 tests/BubbleWidget/mainwindow.ui > tests/BubbleWidget/ui_mainwindow.py

test-widgetcarousel:
	pyuic5 tests/widgetcarousel.ui > tests/ui_widgetcarousel.py

test-hitokoto:
	pyuic5 tests/Hitokoto/mainwindow.ui > tests/Hitokoto/ui_mainwindow.py

test-testMultiComboBox:
	pyuic5 tests/ui_testMultiComboBox.ui > tests/ui_testMultiComboBox.py

pyuic: mainwindow test-mainwindow test-customeProgressbar test-widgetshake test-bubblewidget-mainwindow test-widgetcarousel test-testMultiComboBox
	@echo "处理完成"
	
################### 添加的其它操作(用于处理 pyrcc 内容) ###################
pyrcc:
	pyrcc5 resources.qrc -o $(PROJECT_NAME)/qr_resources.py
	pyrcc5 tests/testMovieGIF/resources.qrc -o tests/testMovieGIF/qr_resources.py

################### 添加的其它操作(构建打包，安装?) ###################

# make dist 将进行构建 whl 文件与代码包
dist: dist-clean
	@echo "Build started"
	python3 ./setup.py sdist bdist_wheel

# make install 将安装到系统(建议使用sudo)
install:
	python3 ./setup.py install

# make install-whl 将安装 all 编译出的分发包
install-whl: 
	python3 -m pip install ./dist/$(PROJECT_NAME)-$(PROJECT_VERSION)-py3-none-any.whl

# make clean 将进行清空一些产生的文件
dist-clean: 
	rm -rf dist build $(PROJECT_NAME).egg-info
